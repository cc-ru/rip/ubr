/*
 *  UDP broadcast relay
 *  Copyright (C) 2018  Dmitry Zhidenkov aka Totoro
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

extern crate pnet;
extern crate whoami;
extern crate clap;

use clap::{App, Arg};

use pnet::datalink::{self, NetworkInterface, DataLinkSender, DataLinkReceiver};
use pnet::datalink::Channel::Ethernet;
use pnet::packet::ethernet::EthernetPacket;
use pnet::util::MacAddr;

fn find_interface(name: &str) -> (Box<DataLinkSender>, Box<DataLinkReceiver>) {
    let interface_name_match =
        |iface: &NetworkInterface| iface.name == name;
    let interfaces = datalink::interfaces();
    let interface = interfaces.into_iter()
        .filter(interface_name_match).next().unwrap();

    match datalink::channel(&interface, Default::default()) {
        Ok(Ethernet(tx, rx)) => (tx, rx),
        Ok(_) => panic!("Unhandled channel type!"),
        Err(e) => panic!("An error occured when creating the datalink channel: {}!", e)
    }
}

fn main() {
    let broadcast_address = MacAddr::new(0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF);

    let matches = App::new("ubr")
        .version("0.1.0")
        .about("Redirects UDP broadcast from one network interface to another")
        .author("Totoro")
        .arg(Arg::with_name("FROM")
            .required(true)
            .help("The name of source network interface"))
        .arg(Arg::with_name("TO")
            .required(true)
            .help("The name of destination network interface"))
        .get_matches();

    let username = whoami::username();

    if username != "root" {
        println!("error: Must be started from under root");
    } else {
        let from = matches.value_of("FROM").unwrap();
        let to = matches.value_of("TO").unwrap();
        let (_, mut receiver) = find_interface(from);
        let (mut sender, _) = find_interface(to);
        println!("Listening for UDP broadcasts on {}...", from);
        loop {
            match receiver.next() {
                Ok(packet) => {
                    let eth_packet = EthernetPacket::new(packet).unwrap();
                    if eth_packet.get_destination() == broadcast_address {
                        println!("{:?}", eth_packet);
                        sender.send_to(packet, None);
                    }
                }
                Err(e) => {
                    panic!("An error occured while reading: {}!", e);
                }
            }
        }
    }
}
