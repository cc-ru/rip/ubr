## UBR
Small tool to redirect UDP broadcasts from one network interface to another.

### Build
Use `cargo` (and stable Rust branch):
```bash
cargo build --release
```

### Usage
The tool needs to be executed from root, because of low level network
interaction.

Syntax:

```bash
ubr <interface A> <interface B>
```

For example:
```bash
sudo ubr wlo1 tap0 
```

All broadcasts from the `wlo1` interface will be redirected to `tap0`.
